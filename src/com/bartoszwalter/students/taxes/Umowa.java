package com.bartoszwalter.students.taxes;

import java.math.BigDecimal;

/**
 * Created by user on 2017-05-18.
 */
public abstract class Umowa {
    protected BigDecimal podstawa;
    protected BigDecimal skladkaEmerytalna; // 9,76% podstawyy
    protected BigDecimal skladkaRentowa; // 1,5% podstawy
    protected BigDecimal ubezpieczenieChorobowe; // 2,45% podstawy
    protected BigDecimal skladkaZdrowotna9procentowa; // od podstawy wymiaru 9%
    protected BigDecimal skladkaZdrowotna7i75procentowa; // od podstawy wymiaru 7,75 %
    protected BigDecimal zaliczkaNaPod; // zaliczka na podatek dochodowy 18%
    protected BigDecimal kosztyUzyskania = new BigDecimal("111.25");
    protected BigDecimal kwotaZmiejsz = new BigDecimal("46.33"); // kwota zmienjszająca podatek 46,33 PLN
    protected BigDecimal zaliczkaUS;
    protected BigDecimal zaliczkaUS0;
    protected String typUmowy;
    protected BigDecimal oPodstawa;
    protected BigDecimal podstawaOpodat;
    protected BigDecimal podatekPotracony;
    protected final BigDecimal oprocentowanieSkladkiEmerytalnej = new BigDecimal("0.0976");
    protected final BigDecimal oprocentowanieSkladkiRentowej = new BigDecimal("0.015");
    protected final BigDecimal oprocentowanieUbzezpieczenieChorobowe = new BigDecimal("0.0245");
    protected final BigDecimal oprocentowanieSkladki9procent = new BigDecimal("0.09");
    protected final BigDecimal oprocentowanieSkladki7i75procent = new BigDecimal("0.0775");
    protected final BigDecimal oprocentowanieZaliczkiNaPodatek = new BigDecimal("0.18");
    protected Umowa nastepny;
    protected char zadanaOperacja;

    public void ustawNastepny(Umowa obiektUmowy) {
        nastepny = obiektUmowy;
    }

    public abstract boolean operacja();

    Umowa(BigDecimal podstawa, char operacja) {
        typUmowy = "UMOWA";
        this.podstawa = podstawa;
        zadanaOperacja = operacja;
    }//Umowa()

    protected void oblicz() {
        oPodstawa = obliczonaPodstawa(podstawa);
        obliczUbezpieczenia(oPodstawa);
        podstawaOpodat = oPodstawa.subtract(kosztyUzyskania).setScale(2, BigDecimal.ROUND_HALF_EVEN);;
    }

    protected void pokazRaport() {
        obliczZaliczke();
        zaliczkaUS0 = zaliczkaUS.setScale(0, BigDecimal.ROUND_HALF_EVEN);
        BigDecimal wynagrodzenie = podstawa
                .subtract(skladkaEmerytalna.add(skladkaRentowa).add(ubezpieczenieChorobowe).add(skladkaZdrowotna9procentowa)
                        .add(zaliczkaUS0));

        System.out.println(typUmowy);
        System.out.println("Podstawa wymiaru składek " + podstawa);
        System.out.println("Składka na ubezpieczenie emerytalne "
                + skladkaEmerytalna);
        System.out.println("Składka na ubezpieczenie rentowe    "
                + skladkaRentowa);
        System.out.println("Składka na ubezpieczenie chorobowe  "
                + ubezpieczenieChorobowe);
        System.out.println("Podstawa wymiaru składki na ubezpieczenie zdrowotne: "
                + oPodstawa);
        System.out.println("Składka na ubezpieczenie zdrowotne: 9% = "
                + skladkaZdrowotna9procentowa + " 7,75% = " + skladkaZdrowotna7i75procentowa);
        System.out.println("Koszty uzyskania przychodu w stałej wysokości "
                + kosztyUzyskania);
        System.out.println("Podstawa opodatkowania " + podstawaOpodat
                + " zaokrąglona " + podstawaOpodat.setScale(0, BigDecimal.ROUND_HALF_EVEN));
        System.out.println("Zaliczka na podatek dochodowy 18 % = "
                + zaliczkaNaPod);
        System.out.println("Kwota wolna od podatku = " + kwotaZmiejsz);
        System.out.println("Podatek potrącony = "
                + podatekPotracony);
        System.out.println("Zaliczka do urzędu skarbowego = "
                + zaliczkaUS + " po zaokrągleniu = "
                + zaliczkaUS0);
        System.out.println();
        System.out.println("Pracownik otrzyma wynagrodzenie netto w wysokości = "
                + wynagrodzenie);
    }

    protected BigDecimal obliczonaPodstawa(BigDecimal podstawa) {
        skladkaEmerytalna = podstawa.multiply(oprocentowanieSkladkiEmerytalnej).setScale(2, BigDecimal.ROUND_HALF_EVEN);
        skladkaRentowa = podstawa.multiply(oprocentowanieSkladkiRentowej).setScale(2, BigDecimal.ROUND_HALF_EVEN);
        ubezpieczenieChorobowe = podstawa.multiply(oprocentowanieUbzezpieczenieChorobowe).setScale(2, BigDecimal.ROUND_HALF_EVEN);
        return podstawa.subtract(skladkaEmerytalna).subtract(skladkaRentowa).subtract(ubezpieczenieChorobowe).setScale(2, BigDecimal.ROUND_HALF_EVEN);
    }//obliczonaPodstawa()

    protected void obliczUbezpieczenia(BigDecimal podstawa) {
        skladkaZdrowotna9procentowa = podstawa.multiply(oprocentowanieSkladki9procent).setScale(2, BigDecimal.ROUND_HALF_EVEN);
        skladkaZdrowotna7i75procentowa = podstawa.multiply(oprocentowanieSkladki7i75procent).setScale(2, BigDecimal.ROUND_HALF_EVEN);
    }//obliczUbezpieczenia()

    protected void obliczPodatek(BigDecimal podstawa) {
        zaliczkaNaPod = podstawa.multiply(oprocentowanieZaliczkiNaPodatek).setScale(2, BigDecimal.ROUND_HALF_EVEN);
    }//obliczPodatek()

    protected void obliczZaliczke() {
        zaliczkaUS = zaliczkaNaPod.subtract(skladkaZdrowotna7i75procentowa).subtract(kwotaZmiejsz);
    }//obliczZaliczke()
}
