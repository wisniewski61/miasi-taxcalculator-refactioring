package com.bartoszwalter.students.taxes;

import java.math.BigDecimal;

/**
 * Created by user on 2017-05-18.
 */
public class UmowaZlecenie extends Umowa {
    protected final char mojaOperacja = 'Z';
    UmowaZlecenie(BigDecimal podstawa, char operacja) {
        super(podstawa, operacja);
        typUmowy = "UMOWA-ZLECENIE";
    }

    @Override
    public boolean operacja() {
        if(mojaOperacja!=zadanaOperacja) {
            if(nastepny==null) return false;
            return nastepny.operacja();
        }//if

        pokazRaport();
        return true;
    }//operacja()

    @Override
    public void pokazRaport() {
        kwotaZmiejsz = new BigDecimal("0");
        oblicz();
        kosztyUzyskania = oPodstawa.multiply(new BigDecimal("0.2")).setScale(2, BigDecimal.ROUND_HALF_EVEN);
        podstawaOpodat = oPodstawa.subtract(kosztyUzyskania).setScale(2, BigDecimal.ROUND_HALF_EVEN);
        obliczPodatek(podstawaOpodat);
        podatekPotracony = zaliczkaNaPod.setScale(2, BigDecimal.ROUND_HALF_EVEN);

        super.pokazRaport();
    }
}
