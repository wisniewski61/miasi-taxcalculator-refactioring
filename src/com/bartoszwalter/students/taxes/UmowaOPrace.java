package com.bartoszwalter.students.taxes;

import java.math.BigDecimal;

/**
 * Created by user on 2017-05-18.
 */
public class UmowaOPrace extends Umowa {
    protected final char mojaOperacja = 'P';

    public UmowaOPrace(BigDecimal podstawa, char operacja) {
        super(podstawa, operacja);
        typUmowy = "UMOWA O PRACĘ";
    }

    @Override
    public boolean operacja() {
        if(mojaOperacja!=zadanaOperacja) {
            if(nastepny==null) return false;
            return nastepny.operacja();
        }//if

        pokazRaport();
        return true;
    }//operacja()

    @Override
    public void pokazRaport() {
        oblicz();
        obliczPodatek(podstawaOpodat);
        podatekPotracony = zaliczkaNaPod.subtract(kwotaZmiejsz).setScale(2, BigDecimal.ROUND_HALF_EVEN);

        super.pokazRaport();
    }//pokazRaport()
}
