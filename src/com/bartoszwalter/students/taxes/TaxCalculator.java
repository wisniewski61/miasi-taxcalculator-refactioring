package com.bartoszwalter.students.taxes;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;

public class TaxCalculator {
	public static double podstawa = 0;
	public static char umowa = ' ';
	private static final char umowaOPraceZnak = 'P';
	private static final char umowaZlecenieZnak = 'Z';

	public static void main(String[] args) {
		if(!pobierzDaneOdUzytkownika())
			return;

		Umowa oprace = new UmowaOPrace(new BigDecimal(podstawa), umowa);
		Umowa zlecenie = new UmowaZlecenie(new BigDecimal(podstawa), umowa);
		oprace.ustawNastepny(zlecenie);
		boolean czyUdana = oprace.operacja();
		
		if(!czyUdana) {
			System.out.println("Nieznany typ umowy!");
		}//else
	}//main()

    private static boolean pobierzDaneOdUzytkownika() {
		try {
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(isr);

			System.out.print("Podaj kwotę dochodu: ");
			podstawa = Double.parseDouble(br.readLine());

			System.out.print("Typ umowy: (P)raca, (Z)lecenie: ");
			umowa = br.readLine().charAt(0);
		} catch (Exception ex) {
			System.out.println("Błędna kwota");
			System.err.println(ex);
			return false;
		}//catch

		return true;
	}//pobierzDaneOdUzytkownika()
}
